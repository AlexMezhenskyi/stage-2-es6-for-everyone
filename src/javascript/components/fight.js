import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    document.addEventListener('keypress', function(event) {
      const firstFighterHealthIndicator = document.getElementById('left-fighter-indicator');
      const firstFighterHealthIndicatorWidth = parseFloat(window.getComputedStyle(firstFighterHealthIndicator).width);
      const secondFighterHealthIndicator = document.getElementById('right-fighter-indicator');
      const secondFighterHealthIndicatorWidth = parseFloat(window.getComputedStyle(secondFighterHealthIndicator).width);

      if (event.code === controls.PlayerOneAttack) {
        let damage = getDamage(firstFighter, secondFighter);

        if (damage) {
          let danagePercent = damage * 100 / secondFighter.health;
          let damageHealthBarWidth = parseFloat(secondFighterHealthIndicatorWidth) / 100 * danagePercent;

          secondFighter.health = secondFighter.health - damage;

          if (secondFighter.health <= 0) {
            secondFighterHealthIndicator.style.width = 0;
            resolve(firstFighter);
          } else {
            secondFighterHealthIndicator.style.width = (secondFighterHealthIndicatorWidth - damageHealthBarWidth) + 'px';
          }
        }
      }

      if (event.code === controls.PlayerTwoAttack) {
        let damage = getDamage(secondFighter, firstFighter);

        if (damage) {
          let danagePercent = damage * 100 / firstFighter.health;
          let damageHealthBarWidth = parseFloat(firstFighterHealthIndicatorWidth) / 100 * danagePercent;

          firstFighter.health = firstFighter.health - damage;

          if (firstFighter.health <= 0) {
            firstFighterHealthIndicator.style.width = 0;
            resolve(secondFighter);
          } else {
            firstFighterHealthIndicator.style.width = (firstFighterHealthIndicatorWidth - damageHealthBarWidth) + 'px';
          }
        }
      }
    })
  });
}

export function getDamage(attacker, defender) {
  // return damage
  let hitPower = getHitPower(attacker);
  let blockPower = getBlockPower(defender);

  if (blockPower > hitPower) {
    return 0;
  }

  return hitPower - blockPower;
}

export function getHitPower(fighter) {
  // return hit power
  return fighter.attack * (Math.random() + 1);
}

export function getBlockPower(fighter) {
  // return block power
  return fighter.defense * (Math.random() + 1);
}
