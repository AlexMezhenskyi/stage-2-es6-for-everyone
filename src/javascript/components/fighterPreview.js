import { createElement } from '../helpers/domHelper';
import versusImg from '../../../resources/versus.png';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  for (let key in fighter) {
    if (key === '_id') {
      continue;
    } else if (key === 'source') {
      const image = createElement({
        tagName: 'img',
        className: `fighter-preview___item ${key}`,
        attributes: { src: fighter[key] },
      });

      fighterElement.append(image);
    } else {
      const fighterInfoItem = createElement({
        tagName: 'div',
        className: `fighter-preview___item ${key}`,
      });

      fighterInfoItem.innerHTML += `${key.toLocaleUpperCase()}: <span>${fighter[key]}</span>`;
      fighterElement.append(fighterInfoItem);
    }
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
